<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Filesystem\Factory;

use Illuminate\Http\Request;

use DB;
use Auth;

use App\UserPost;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user=DB::table('users')
				->get();

		$user_id = Auth::user()->id;	
		
		$userpost=DB::table('user_post')
				->orderby('id','desc')
				->where('user_id',$user_id)
				->get();
				
		return view('home',compact('userpost'));
	}

	public function addPost(Request $request)
	{
		$user_id=$request->get('user_id');
		$post=$request->get('post');

		$postvalues = new UserPost;
		$postvalues->post=$post;
		$postvalues->user_id=$user_id;
		
		$postvalues->save();

		return redirect()->action('HomeController@index');
	}

	public function editPost(Request $request)
	{
		$id=$request->get('id');
		$user_id=$request->get('user_id');
		
		$userpost=DB::table('user_post')
				->orderby('id','desc')
				->where('user_id',$user_id)
				->get();
		$mypost=DB::table('user_post')
				->where('id',$id)
				->where('user_id',$user_id)
				->get();
				
		return view('edithome',compact('mypost','userpost'));
	}

	public function updatePost(Request $request)
	{
		$post=$request->get('post');
		$id=$request->get('id');
		$user_id=$request->get('user_id');

		DB::table('user_post')
			->where('id',$id)
			->where('user_id',$user_id)
			->update(['post'=>$post]);

		return redirect()->action('HomeController@index');

	}

	public function deletePost(Request $request)
	{

		$id=$request->get('id');
		$user_id=$request->get('user_id');
		

		Db::table('user_post')
			->where('id',$id)
			->where('user_id',$user_id)
			->delete();

			return redirect()->action('HomeController@index');
	}

}
