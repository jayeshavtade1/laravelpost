$(function() {

  /*======================================
  =            Date Picker initializers  =
  ======================================*/

  // Initializing the date picker for joing-date
  initDatePicker('joining-date-picker', 'joining-date');

  // Initializing the date picker for end-date
  initDatePicker('end-date-picker', 'end-date');

  // Initializing the date picker for Purchase Order
  initDatePicker('bill_date_picker', 'billdate');

  // Initializing the date picker for Sales Order
  initDatePicker('date_of_order_picker', 'date_of_order');  

  // Initializing the date picker for Date of delievery
  initDatePicker('date_of_delivery_picker', 'date_of_delivery');

  // Initializing the date picker for Date of delievery in purchase order
  initDatePicker('delivery_date_picker', 'delivery_date');

  /*-----  End of Master Data HR  ------*/



  /*===============================================
  =            Common Utility functios            =
  ===============================================*/
  
  /**
    *
    * Function to initialize the datepicker functionality
    *
    **/
      

  function initDatePicker(datePickerID, dateInput){
    $( "#" + datePickerID )
      
    // Initializing the datepicker
    .datepicker()

    // Detecting the on change date event
    .on('changeDate', function (data) {
      var date = new Date(data.date);
      var dateString = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();
      $("#" + dateInput).val(dateString);
    });  
  }  
  
  /*-----  End of Common Utility functios  ------*/
});
