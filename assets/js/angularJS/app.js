var app = angular.module('myApp', []);

app.controller('MyController', function($scope) {
      /*$scope.clock = new Date();
      var updateClock = function() {
                $scope.clock = new Date();
      };
      setInterval(function() {
          $scope.$apply(updateClock);
      }, 1000);
updateClock();*/
});


app.controller('fCtrl', function($scope){

  $scope.names = [

  {id:1}
  ];
  
  $scope.add = function(){

    var tmp =  $scope.names[0];

    
    $scope.names.push(tmp) 
    
  }
  
});


app.directive('contactType', function() {
	var url='../../resources/views/master/patient/ContactType.php';
  return {
    restrict: "E",
    scope: {},
    templateUrl:url,
    controller: function($rootScope, $scope, $element) {
      $scope.contacts = $rootScope.GetContactTypes;
      $scope.Delete = function(e) {
        //remove element and also destoy the scope that element
        $element.remove();
        $scope.$destroy();
      }
    }
  }
});

app.controller("EmployeeCtrl", ['$rootScope', '$scope', '$compile', '$element', '$filter', 'ContactTypesService', function($rootScope, $scope, $compile, $element, $filter, ContactTypesService) {

  $rootScope.GetContactTypes = ContactTypesService.ContactTypes();
  
  var employee={firstName:'',lastName:'',contacts:''}
  var employeeList = [];
  $scope.AddContactTypeControl = function() {
    var divElement = angular.element(document.querySelector('#contactTypeDiv'));
    var appendHtml = $compile('<contact-Type></contact-Type>')($scope);
    divElement.append(appendHtml);
  }
  var GetContactType = function(id) {
    return $filter('filter')($rootScope.GetContactTypes, {
      contactId: id
    })[0].contactType;
  }
  var retriveValue = function() {
    // http://stackoverflow.com/questions/12649080/get-to-get-all-child-scopes-in-angularjs-given-the-parent-scope
    var UserContacts = [];
    var ChildHeads = [$scope.$$childHead];
    var currentScope;
    while (ChildHeads.length) {
      currentScope = ChildHeads.shift();
      while (currentScope) {
        if (currentScope.ContactType !== undefined)
          UserContacts.push({
            ContactType: GetContactType(currentScope.ContactType),
            ContactValue: currentScope.ContactValue
          });

        currentScope = currentScope.$$nextSibling;
      }
    }
    return UserContacts;
  }

  var ClearControls = function(allContacts) {
    var i;
    for (i = 0; i < allContacts.length; i++)
      angular.element(allContacts[i]).remove();

    $scope.employee = '';
    $scope.employee.contacts = '';
    $scope.$$childHead = $scope.$new(true);

  }
  var val = 0;
  $scope.OnSave = function(emp) {

   if(emp!==null && emp!==undefined){
    emp.id = ++val;
    emp.contacts = retriveValue($scope);
    employeeList.push(emp);
    $scope.EmployeeList = employeeList;
    console.log($scope.EmployeeList);
    var allContacts = angular.element(document.getElementsByTagName("contact-Type"));
    ClearControls(allContacts);
  }
}

}]);



app.service("ContactTypesService", [function() {
  var list = [];
  return {
    ContactTypes: function() {
      list.push({contactId: 1,contactType: 'Mobile'});
      list.push({contactId: 2,contactType: 'Office'});
      list.push({contactId: 3,contactType: 'Home'});
      list.push({contactId: 4,contactType: 'Fax'});
      list.push({contactId: 5,contactType: 'Landline'});
      list.push({contactId: 6,contactType: 'Other'});
      return list;
    }
  }
}]);

var empid = 1;
app.controller('quotationCtrl', function($scope){
  $scope.employees = [];

//TO CHECK EMPTY VALUE
  $scope.newEmployee = [desc => null,cost => null];

  $scope.saveRecord = function () {

    if($scope.newEmployee.desc != null && $scope.newEmployee.cost != null) {
      if($scope.newEmployee.id == null) {
        $scope.newEmployee.id = empid++;
        $scope.employees.push($scope.newEmployee);
      } else {

        for (i in $scope.employees) {
          if ($scope.employees[i].id == $scope.newEmployee.id) {
            $scope.employees[i] = $scope.newEmployee;
          }
        }
      }
      /* console.log("before"+$scope.newEmployee['category']);*/
      $scope.newEmployee = {};
      /*console.log("after"+$scope.newEmployee['category']);*/
    }
    else
    {
      window.alert("Please, Enter Proper Value!!");
      $scope.newEmployee = [];
    }//close if-else null
  }

  $scope.delete = function (id) {

    for (i in $scope.employees) {
      if ($scope.employees[i].id == id) {
        $scope.setTotals($scope.employees[i].cost,i);
        /*console.log($scope.employees[i].cost);*/
      }
    }

    for (i in $scope.employees) {
      if ($scope.employees[i].id == id) {
        $scope.employees.splice(i, 1);
        $scope.newEmployee = {};
      }
    }

  }

  $scope.edit = function (id) {

    for (i in $scope.employees) {
      if ($scope.employees[i].id == id) {

        $scope.newEmployee = angular.copy($scope.employees[i]);
      }
    }
  }

/*$scope.getTotal = function(){
    var total = 0;
    var array = $scope.employees;
    console.log(array);
    if(array != null)
    {
    for(var i = 0; i < array.length; i++){
        var product = $scope.employees.cost[i];
        total += employees.cost;
    }
    return total;
  }
}*/

$scope.cost = 0;

$scope.setTotals = function(item,delIndex){

  if(delIndex == undefined)
  {
    $scope.cost += parseInt(item.cost);

  }else{
    $scope.cost -= parseInt(item);
  }
}

/* $scope.pop = function(){
        toaster.pop('success', "title", "text");
        toaster.pop('error', "title", "text");
        toaster.pop('warning', "title", "text");
        toaster.pop('note', "title", "text");
    };*/
});