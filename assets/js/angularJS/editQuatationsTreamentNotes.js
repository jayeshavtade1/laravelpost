var app = angular.module('myApp', []);

var empid = 1;
app.controller('editQuotationCtrl', function($scope){
/*  $scope.employees = myPathVar;

  for(var i in $scope.employees)
  {
  	$scope.employees[i].id = empid;
  	$scope.employees[i].desc_id = $scope.employees[i]['id'];
  	$scope.employees[i].desc = $scope.employees[i]['description'];
  	$scope.employees[i].cost = $scope.employees[i]['cost'];
  	empid++;
  }*/

  $scope.employees = [];

//TO CHECK EMPTY VALUE
  $scope.newEmployee = [desc => null,cost => null];

  $scope.saveRecord = function () {

    if($scope.newEmployee.desc != null && $scope.newEmployee.cost != null) {
      if($scope.newEmployee.id == null) {
        $scope.newEmployee.id = empid++;
        $scope.newEmployee.check = 'newValue';
        $scope.employees.push($scope.newEmployee);
      } else {

        for (i in $scope.employees) {
          if ($scope.employees[i].id == $scope.newEmployee.id) {
            $scope.employees[i] = $scope.newEmployee;
          }
        }
      }
      /* console.log("before"+$scope.newEmployee['category']);*/
      $scope.newEmployee = {};
      /*console.log("after"+$scope.newEmployee['category']);*/
    }
    else
    {
      window.alert("Please, Enter Proper Value!!");
      $scope.newEmployee = [];
    }//close if-else null
  }

  $scope.delete = function (id) {

    for (i in $scope.employees) {
      if ($scope.employees[i].id == id) {
        $scope.setTotals($scope.employees[i].cost,i);
        /*console.log($scope.employees[i].cost);*/
      }
    }

    for (i in $scope.employees) {
      if ($scope.employees[i].id == id) {
        $scope.employees.splice(i, 1);
        $scope.newEmployee = {};
      }
    }

  }

  $scope.edit = function (id) {

    for (i in $scope.employees) {
      if ($scope.employees[i].id == id) {

        $scope.newEmployee = angular.copy($scope.employees[i]);
      }
    }
  }

/*$scope.getTotal = function(){
    var total = 0;
    var array = $scope.employees;
    console.log(array);
    if(array != null)
    {
    for(var i = 0; i < array.length; i++){
        var product = $scope.employees.cost[i];
        total += employees.cost;
    }
    return total;
  }
}*/

$scope.cost = 0;

$scope.setTotals = function(item,delIndex){

  if(delIndex == undefined)
  {
    $scope.cost += parseInt(item.cost);

  }else{
    $scope.cost -= parseInt(item);
  }
}

/* $scope.pop = function(){
        toaster.pop('success', "title", "text");
        toaster.pop('error', "title", "text");
        toaster.pop('warning', "title", "text");
        toaster.pop('note', "title", "text");
    };*/
});