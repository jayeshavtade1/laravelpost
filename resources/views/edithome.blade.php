@extends('app')

<style>
	.post{
		border:1px solid #DCDCDC;
		border-radius:5px;
		margin:10px;
		padding:25px;
	}
</style>

@section('content')

<div class="container">


	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					Welcome, {{ Auth::user()->name }} !
					<br><br>
					{!! Form::open(array('route'=>'updatepost','class' => 'form')) !!}

						<div class="row">
							<div class="col-xs-8">
								{!! Form::textarea('post',$mypost['0']->post,['class'=>'form-control', 'rows' => 5, 'cols' => 10,'placeholder' =>'Write your post here..']) !!}<br>
								<input type="hidden" value="{{$mypost['0']->id}}" name="id">
								<input type="hidden" value="{{$mypost['0']->user_id}}" name="user_id">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-7">
							</div>
							<div class="col-xs-4">
								{!! Form::button('Save',array('type' => 'submit','class'=>'btn btn-info')) !!}
							</div>
						</div>
					{!! Form::close() !!}

					@foreach($userpost as $post)
					<div class="post">
						<div class="row">
							<div class="col-md-8 row">{{$post->post}}</div>
							
						</div>
						
					</div>
					@endforeach


				</div>
			</div>
		</div>



	</div>
</div>
@endsection
