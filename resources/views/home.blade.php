@extends('app')

<style>
	.post{
		border:1px solid #DCDCDC;
		border-radius:5px;
		margin:10px;
		padding:25px;
	}
</style>

@section('content')

<div class="container">





	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					Welcome, {{ Auth::user()->name }} !
					<br><br>
					{!! Form::open(array('route'=>'addpost','class' => 'form')) !!}

						<div class="row">
							<div class="col-xs-8">
								{!! Form::textarea('post',null,['class'=>'form-control', 'rows' => 5, 'cols' => 10,'placeholder' =>'Write your post here..']) !!}<br>
							</div>
						</div>
						
						
						<div class="row">
							<div class="col-xs-7"></div>
							<div class="col-xs-1">
								{!! Form::button('Post',array('type' => 'submit','class'=>'btn btn-info')) !!}
								<input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
							</div>
						</div>
					{!! Form::close() !!}

					@foreach($userpost as $post)
					<div class="post">
						<div class="row">
							<div class="col-md-8 row">{{$post->post}}</div>
							
							
						</div>
						<div class="row">
							<div class="col-md-8"></div>
							<div class="col-md-1">
								{!! Form::open(array('route'=>'editpost','class' => 'form')) !!}
									{!! Form::button('Edit',array('type' => 'submit','class'=>'btn btn-xs btn-primary')) !!}
									<input type="hidden" value="{{$post->id}}" name="id">
									<input type="hidden" value="{{$post->user_id}}" name="user_id">
								{!! Form::close() !!}
							</div>
							<div class="col-md-1">
								{!! Form::open(array('route'=>'deletepost','class' => 'form')) !!}
									{!! Form::button('Delete',array('type' => 'submit','class'=>'btn btn-xs btn-danger','id'=>'deletebutton')) !!}
									<input type="hidden" value="{{$post->id}}" name="id">
									<input type="hidden" value="{{$post->user_id}}" name="user_id">
								{!! Form::close() !!}
							</div>
						</div>
					</div>
					@endforeach


				</div>
			</div>
		</div>



	</div>
</div>
@endsection
